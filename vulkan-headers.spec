%global __python %{__python3}
Name:           vulkan-headers
Version:        1.3.290.0
Release:        2
Summary:        Vulkan Header files and API registry
License:        Apache-2.0
URL:            https://github.com/KhronosGroup/Vulkan-Headers
Source0:        %url/archive/vulkan-sdk-%{version}/Vulkan-Headers-vulkan-sdk-%{version}.tar.gz
BuildRequires:  cmake

BuildArch:      noarch

%description
Vulkan is a new generation graphics and compute API that provides high-efficiency, cross-platform
access to modern GPUs used in a wide variety of devices from PCs and consoles to mobile phones
and embedded platforms.
This package includes Vulkan Header files and API registry.

%prep
%autosetup -n Vulkan-Headers-vulkan-sdk-%{version} -p1

%build
%cmake -DCMAKE_INSTALL_LIBDIR=%{_libdir}
%cmake_build

%install
%cmake_install

%files
%doc README.md
%dir %{_datadir}/vulkan/
%{_datadir}/vulkan/registry/
%{_includedir}/vulkan/
%{_includedir}/vk_video/
%{_datadir}/cmake/VulkanHeaders/

%changelog
* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 1.3.290.0-2
- adopt to new cmake macro

* Mon Aug 26 2024 dillon chen <dillon.chen@gmail.com> - 1.3.290.0-1
- Update to release 1.3.290.0 
- because check_spec_file bug, remove version some letters

* Wed Jun 19 2024 yaoxin <yao_xin001@hoperun.com> - 1.3.283.0-1
- Update to release SDK-1.3.283.0

* Thu Aug 24 2023 dillon chen <dillon.chen@gmail.com> - sdk-1.3.261.0-1
- Update to sdk-1.3.261.0

* Fri Jul 21 2023 dillon chen <dillon.chen@gmail.com> - 1.3.257-1
- github tag is 1.3.257 

* Tue Jul 11 2023 yaoxin <yao_xin001@hoperun.com> - 1.3.257.0-1
- Update to 1.3.257.0

* Wed Jan 08 2020 zhouyihang <zhouyihang1@huawei.com> - 1.1.92.0-2
- Package init
